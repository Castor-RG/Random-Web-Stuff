window.addEventListener('keydown', function(e)
{
	const key = document.querySelector(`div[data-key="${e.keyCode}"]`);
	const audio = document.querySelector(`audio`);
	var random = Math.floor(Math.random()*3);
	
	if(!key)
	{return;}
    
    audio.currentTime = 0;
	audio.play();
    key.classList.add("pressed");
    const keys = document.querySelectorAll('.key');
	keys.forEach(key => key.addEventListener('transitionend', removeTransition));
});

function removeTransition(e)
{
  if(e.propertyName !== 'transform') return;
  console.log(e.keyCode + " transition ended");
  this.classList.remove('pressed');
}